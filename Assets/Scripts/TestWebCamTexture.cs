using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class TestWebCamTexture : MonoBehaviour
{
    public TMP_Dropdown dropdown;
    public RawImage camImg;

    private Quaternion imgBaseRotation;
    private WebCamDevice[] devices;
    private WebCamTexture webcamTexture;
    private AspectRatioFitter ratioFitter;

    private int deviceIdx = 0;

    // Start is called before the first frame update
    private void Start()
    {
        ratioFitter = camImg.GetComponent<AspectRatioFitter>();
        imgBaseRotation = camImg.transform.rotation;

        devices = WebCamTexture.devices;
        List<string> deviceList = new List<string>();
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);
            deviceList.Add(devices[i].name);
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(deviceList);
        dropdown.onValueChanged.AddListener(OnDropDownChanged);

        UpdateWebCam(deviceIdx);
    }

    // Update is called once per frame
    private void Update()
    {/*
        if (webcamTexture != null &&
            webcamTexture.isPlaying)
        {
            transform.rotation = imgBaseRotation * Quaternion.AngleAxis(webcamTexture.videoRotationAngle, Vector3.up);
        }*/
    }

    private void OnDropDownChanged(int value)
    {
        UpdateWebCam(value);
    }

    private void UpdateWebCam(int idx)
    {
        deviceIdx = Mathf.Clamp(idx, 0, devices.Length - 1);
        if (webcamTexture != null)
        {
            camImg.enabled = false;
            webcamTexture.Stop();
            Destroy(webcamTexture);
        }

        webcamTexture = new WebCamTexture(devices[deviceIdx].name);

        Debug.Log($"Update camera to {devices[deviceIdx].name}");

        camImg.material.mainTexture = webcamTexture;
        camImg.enabled = true;

        webcamTexture.Play();

        float angle = AdjustRatation(camImg.transform, webcamTexture);
        FitImageSizeToCamSize(
            (Mathf.Abs(angle)==90)?AspectRatioFitter.AspectMode.HeightControlsWidth:AspectRatioFitter.AspectMode.WidthControlsHeight, 
            webcamTexture);
    }

    private void FitImageSizeToCamSize(AspectRatioFitter.AspectMode mode, WebCamTexture camTexture)
    {
        float ratio = (float)camTexture.width / (float)camTexture.height;

        ratioFitter.aspectMode = mode;
        ratioFitter.aspectRatio = ratio;
        Debug.Log($"camTexture:{camTexture.width} {camTexture.height}");
    }

    float AdjustRatation(Transform obj, WebCamTexture camTexture)
    {
        float angle = camTexture.videoRotationAngle;
        obj.rotation = imgBaseRotation * Quaternion.AngleAxis(angle, Vector3.back);
        Debug.Log($"camTexture.videoRotationAngle:{angle}");

        return angle;
    }
}